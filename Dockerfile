FROM mcr.microsoft.com/mssql/server
COPY orientation.bak /var/opt/mssql/data/
COPY *.sh /
ENV DB_FILENAME=orientation.bak DB_NAME=Orientation SA_PASSWORD=Orientation1 ACCEPT_EULA=y
ENTRYPOINT /entrypoint.sh
