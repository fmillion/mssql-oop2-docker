#!/bin/sh

# wait for the server to start
sleep 30

# load the data
echo "Loading bundled data."
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d master -q "RESTORE DATABASE [$DB_NAME] FROM DISK = '/var/opt/mssql/data/$DB_FILENAME' WITH RECOVERY"
echo "Loading completed."

rm /var/opt/mssql/data/$DB_FILENAME

