# .NET Core API Workshop

Welcome! This is the .NET Core Developer Workshop. In this workshop you will build a very simple Web API that can retrieve content from a database. This workshop will not focus on building a frontend.

## Getting Started

You will need the following software on your computer to get started.

* **Windows 10**. (This is a Windows-only workshop at this time.)
* **Visual Studio**. Either 2017 or 2019 is suitable, and the Community edition is OK but higher editions will work fine. If you need Visual Studio, go to [https://www.visualstudio.com](https://www.visualstudio.com) to download it.
* **.NET Core SDK 2.2**. If you're on Visual Studio 2019, use [this version](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.2.300-windows-x64-installer); if you're on Visual Studio 2017, use [this version](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.2.105-windows-x64-installer).
* **SQL Server Management Studio**. Use [this link](https://go.microsoft.com/fwlink/?linkid=2094583) to immediately start downloading the current version.
* **Docker Desktop for Windows**. To avoid needing to sign up for a Docker Hub account, use [this direct link](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe) to download Docker.
    * Note that Docker for Windows will interfere with virtual machine software. If you use VMware or VirtualBox, you may have problems using those tools after installing and using Docker Desktop. The instructor can explain this issue further if it is relevant to you and offer some possible solutions.
* **Git for Windows**. Download it from [https://git-scm.com/download/win](here).
    * When installing Git for Windows, it is strongly recommended you choose the "Checkout as-is, commit as-is" option when prompted for line ending handling. Failing to use this option is likely to cause issues when you work with Dockerfiles and related Docker commands.
    * If you already have Git Bash installed, you can effect this change by typing this command at a terminal: `git config --global core.autocrlf false`
* **Postman**. Download it at [https://www.getpostman.com/](https://www.getpostman.com/). This tool is very useful for testing your API while you are writing it.

## Project Goal

The goal for this project is to design a simple backend API for a SQL Server database. The SQL Server database is the infrastructure for an Online Orientation system. For this exercise, you'll write a few simple methods that read, update and validate data in the database:

* One method to retrieve a page of content.
* One method to authenticate a user and provide a JWT (JavaScript Web Token) that a frontend might store and use for later authentication
* One method (requiring authentication) to add a new user to the database.
* One method (requiring authentication) to modify a content page.

### Extra Credit

If you have time after completing the above, try using Swagger and Swashbuckle to write some API documentation for the methods you have written. [https://aspnetcore.readthedocs.io/en/stable/tutorials/web-api-help-pages-using-swagger.html](This tutorial) is a great place to get started. Swagger documentation is accessible from a web browser and, if well written, provides a very clean and intuitive interface for other developers to reference your work and learn how to interact with your API.

## A little background on Docker

If you're familiar with Docker, you can skip this section.

Docker is a *container platform*. It is a system that allows developers to package up software and data into *images*, which are then run by server administrators or other developers within *containers*. 

Docker is a complex topic and many books have been written on the subject, so we will keep this summary brief. The key thing to know how to do as a developer is to create a Docker image from a `Dockerfile` (a text file specifying to the Docker engine how it should compile an image) and how to run that image in a container.

If you are familiar with virtual machines, you can compare the *image* to an ISO image containing an operating system, and the *container* to the virtual machine. However, unlike virtual machines, containers need minimal or no installation other than downloading the image. Docker handles the job of creating an isolated environment for the container, copying the image into that environment, and running it. 

One of Docker's strengths is its ability to easily set up and run *multiple* containers based on a single image, and also to run those containers in any environment that supports Docker regardless of the host operating system. On Windows, Docker Desktop actually runs Linux-based container images on Windows. This means that software written for Linux is able to be run on Windows, and it can be started up in seconds.

In this workshop, you will first clone a Git repository containing a Dockerfile and some supporting files. You will create a Docker image using this data, and then you will run the image as a container. That container will provide a Microsoft SQL Server on your system with all of the data needed for the project preloaded. Once the container is built, you'll be quite impressed at how you can bring up your database server in mere seconds.

## Setting up the Docker image

The first thing you'll need to do is setup the Docker image that contains the testing database. The testing database comes preloaded with a schema and some data. Your API will be working with the data in this database. 

For this part of the workshop, you'll be using a command prompt to interact with Docker. The standard Windows command line (`cmd.exe`) is recommended, but Git Bash should also work fine. If you do experience any unusual problems try again using the Windows command line. (The example code in this workshop will be written using the Windows command line)

Follow these steps to download, build and setup your database:

1. Open a command prompt.
2. Optionally, change into a more convenient directory. cmd.exe will place you in your home directory by default (usually C:\Users\\[your username\]). You may want to first change into somewhere such as the Documents folder, so you'll be able to find your code folder more easily in Windows Explorer. You can simply type `cd Documents` to get there. 
3. Clone the Git repository by typing `git clone https://bitbucket.org/fmillion/mssql-oop2-docker/src`.
4. Change into the cloned code directory: `cd mssql-oop2-docker`
5. We will now build the Docker image. To do this, type: `docker build -t oop .`
    * `docker`: The docker command.
    * `build`: Instructs the Docker engine to build an image. Docker will look in the given directory for a file called `Dockerfile` for instructions. 
    * `-t oop`: Names (tags) the image as "oop". 
    * `.`: Specifies that Docker should look in the current directory for the Dockerfile to build.

    The build process will take only a few minutes at most. You will see many messages flash across your screen as the SQL Server base image is retrieved from Microsoft and the data is loaded into the image and configured. If you see "`Successfully tagged oop:latest`", then your image build was successful and you can proceed.
6. Now we will start up your database server. The command is: `docker run -d --name oop -p 1433:1433 oop`
    * `docker`: The docker command.
    * `run`: Instructs Docker to start a new container.
    * `-d`: Tells Docker that this container should run in the background as a service. If you do not specify this, you will see diagnostic messages from SQL Server, but will need to end the server using Ctrl+C (which will stop the SQL Server) before being able to enter more commands. With `-d` you will receive a command prompt immediately and can then interact with the container later using further `docker` commands as described later.
    * `--name oop`: Gives this container a name so that you can reference it later. If you don't give this parameter, Docker generates a randon name out of two dictionary words, and you'll need to use another command to view the name of the container. 
        
        You can specify a name other than `oop`, but you will need to substitute that name for any other command where the name of the container is given.
    
    * `-p 1433:1433`: Tells the Docker engine that you want to provide access to a service running in the container. SQL Server runs on port 1433, so this command says "provide access to port 1433 in the container, via port 1433 on my computer". 
        
        If you're already running SQL Server on your computer, this command will fail. You can get around this by changing the *first* of the two port numbers to something else, say `1434`. If you do this, you'll need to remember this when you reference the server later in connection strings or in SSMS.

    * `oop`: The name of the image to start.

7. Wait about 30 seconds after the prompt returns, and then launch SQL Server management studio.
8. Now you can try to connect to your new SQL Server and see the database. In SSMS, enter `localhost` as the server address, `sa` as the username, and `Orientation1` as the password. 
    * If you needed to run SQL Server on a non-default port, include that port number after `localhost`, separating the two with a comma. For example: `localhost,1434`
9. If you're able to connect and can see an `Orientation` database, you're all set! You've just installed and started SQL Server along with a preloaded database!

## Basic Guidelines

Once again, the endpoints required for this workshop are:

* One method to retrieve a page of content.
* One method to authenticate a user and provide a JWT (JavaScript Web Token) that a frontend might store and use for later authentication
* One method (requiring authentication) to add a new user to the database.
* One method (requiring authentication) to modify a content page.

These are the specific requirements for the Web API project:

1. Use ASP.Net Core to write your Web API. Do not use classic .NET 4.x. Do not worry about writing any frontend pages.
2. Use Entity Framework Core to "scaffold" a model using the existing database. (See the Tips section for details on how to build a connection string, which will be part of the required steps to scaffold a model.)
3. For each endpoint, define both the input requirements and the response payload format. Response payloads, except when they are primitives (a single string, integer, etc), should be defined as a class, and the return value for such methods should be an instance of that class. Similarly for input data, except in the case where a single value is the input, input should be a class and that class should be expected in the body of a POST request.
4. Follow REST philosophy. 
    * For the method that gets content, use a GET request on `/api/content/<id>`, with `id` being an integer representing the content ID.
    * For the method that authenticates a user, use a POST request to `/api/user/login`.
    * For the method that adds a user, use a POSt request to `/api/user/add`.
    * And so on...
5. Use LINQ queries to interact with the database. It is preferable to use the Lambda syntax (example: `dbContext.User.Where(x => x.UserName=="test")).FirstOrDefault()`) rather than the query language style syntax.
6. Use Postman to test your endpoints as you write them. This lets you make sure your code is functioning correctly.

## Hints

Here are a random selection of hints that should help you along the way:

* Content is stored in the `orientation.content` table. The field `contentId` is a unique ID for a content page. `contentTitle` and `contentData` are the key values you'll need to return - other values you can ignore for this exercise. `contentData` is Markdown formatted data - "bonus kudos" if you can provide the data both as Markdown as well as translated to HTML.
* User authentication should be done against the `orientation.user` table. *Make sure you properly encrypt passwords when storing them in the database - do not store plaintext passwords!* You may use built in .NET methods and you are also free to use external encryption libraries such as BCrypt.Net.
* To form a connection string for Entity Framework, use this value: `data source=localhost; user id=sa; password=Orientation1; initial catalog=Orientation`. 
    * If you needed to run SQL Server on an alternative port, change `data source=localhost;` to `data source="localhost,1434";` (1434, or whichever port you chose)
* If you need to restart your database in Docker, such as if you rebooted your computer during the exercise, use the command `docker start oop`. You won't be able to use `docker run` since the container already exists - this is good though because it means any data you may have modified will still be present. You can also stop the database manually if you desire with `docker stop oop`. (Replace `oop` with whatever name you gave the container if you chose something other than `oop` when initially starting.)
* If you want to restart the container and reset its data to its initial state, losing any changes you've made to the database, first stop the container (`docker stop oop`), then delete it (`docker rm oop`). Then you can run the `docker run` command again from above.
* You're free to go online and look up any assistance you need, and you can also ask the instructor for help if needed. 

## Resources

* [JWT Web Tokens examples](https://jasonwatmore.com/post/2018/08/14/aspnet-core-21-jwt-authentication-tutorial-with-example-api)
* [Using ASP.NET Core with Existing Database (Scaffolding)](https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/existing-db)
